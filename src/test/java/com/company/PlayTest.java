package com.company;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PlayTest {
    Play play;

    /**
     * @Test – определяет что метод method() является тестовым.
     * @beforeEach – указывает на то, что метод будет выполнятся перед каждым тестируемым методом @Test.
     * @afterEach – указывает на то что метод будет выполнятся после каждого тестируемого метода @Test
     * @beforeAll – указывает на то, что метод будет выполнятся в начале всех тестов,
     * а точней в момент запуска тестов(перед всеми тестами @Test).
     * @afterAll – указывает на то, что метод будет выполнятся после всех тестов.
     * @Ignore – говорит, что метод будет проигнорирован в момент проведения тестирования.
     * (expected = Exception.class) – указывает на то, что в данном тестовом методе
     * вы преднамеренно ожидаете Exception.
     * (timeout = 100) – указывает, что тестируемый метод не должен занимать больше чем 100 миллисекунд.
     */

    @BeforeEach
    void init() {
        Form form = new Form();
        form.setMathematical_expectation_Excavator(4f);
        form.setMathematical_expectation_Bulldozer(6f);
        form.setProfit_Excavator(5000);
        form.setProfit_Bulldozer(3000);
        form.setLoss_Excavator(5000);
        form.setLoss_Bulldozer(3000);
        form.setRepair_time_Excavator_Mechanic3(3f);
        form.setRepair_time_Bulldozer_Mechanic3(-1f);
        form.setRepair_time_Excavator_Mechanic6(1);
        form.setRepair_time_Bulldozer_Mechanic6(2);
        form.setRepair_time_Excavator_All(0.25f);
        form.setRepair_time_Bulldozer_ALL(1.5f);
        form.setPay_Mechanic3(600);
        form.setPay_Mechanic6(1000);
        form.setPay_Brigade(500);
        form.setUse_Mechanic3(true);
        form.setUse_Mechanic6(true);
        play = new Play(form);

    }

    @Test
    void step() {
        play.step();
        Assertions.assertEquals((int) (play.getIdle_time_Bulldozer() + play.getWork_time_Bulldozer()), (int) play.getWorkDayDuration());
        Assertions.assertEquals((int) (play.getIdle_time_Excavator() + play.getWork_time_Excavator()), (int) play.getWorkDayDuration());
    }

    @Test
    void days() {
        for (int i = 0; i < 1000; i++) {
            play.step();
            play.clean();
        }
        System.out.println("Money per day: " + play.getMoney() / 1000f);
        Assertions.fail();
    }
}