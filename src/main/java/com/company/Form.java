package com.company;

public class Form {
    private float mathematical_expectation_Excavator = 0;
    private float mathematical_expectation_Bulldozer = 0;
    private float profit_Excavator = 0;
    private float profit_Bulldozer = 0;
    private float loss_Excavator = 0;
    private float loss_Bulldozer = 0;
    private float repair_time_Excavator_Mechanic3 = 0;
    private float repair_time_Bulldozer_Mechanic3 = 0;
    private float repair_time_Excavator_Mechanic6 = 0;
    private float repair_time_Bulldozer_Mechanic6 = 0;
    private float repair_time_Excavator_All = 0;
    private float repair_time_Bulldozer_ALL = 0;
    private float pay_Mechanic3 = 0;
    private float pay_Mechanic6 = 0;
    private float pay_Brigade = 0;
    private float work = 16;
    private float sleep = 8;
    private int days = 1;
    private boolean use_Mechanic3 = false;
    private boolean use_Mechanic6 = false;

    public float getMathematical_expectation_Excavator() {
        return mathematical_expectation_Excavator;
    }

    public void setMathematical_expectation_Excavator(float mathematical_expectation_Excavator) {
        this.mathematical_expectation_Excavator = mathematical_expectation_Excavator;
    }

    public float getMathematical_expectation_Bulldozer() {
        return mathematical_expectation_Bulldozer;
    }

    public void setMathematical_expectation_Bulldozer(float mathematical_expectation_Bulldozer) {
        this.mathematical_expectation_Bulldozer = mathematical_expectation_Bulldozer;
    }

    public float getProfit_Excavator() {
        return profit_Excavator;
    }

    public void setProfit_Excavator(float profit_Excavator) {
        this.profit_Excavator = profit_Excavator;
    }

    public float getProfit_Bulldozer() {
        return profit_Bulldozer;
    }

    public void setProfit_Bulldozer(float profit_Bulldozer) {
        this.profit_Bulldozer = profit_Bulldozer;
    }

    public float getLoss_Excavator() {
        return loss_Excavator;
    }

    public void setLoss_Excavator(float loss_Excavator) {
        this.loss_Excavator = loss_Excavator;
    }

    public float getLoss_Bulldozer() {
        return loss_Bulldozer;
    }

    public void setLoss_Bulldozer(float loss_Bulldozer) {
        this.loss_Bulldozer = loss_Bulldozer;
    }

    public float getRepair_time_Excavator_Mechanic3() {
        return repair_time_Excavator_Mechanic3;
    }

    public void setRepair_time_Excavator_Mechanic3(float repair_time_Excavator_Mechanic3) {
        this.repair_time_Excavator_Mechanic3 = repair_time_Excavator_Mechanic3;
    }

    public float getRepair_time_Bulldozer_Mechanic3() {
        return repair_time_Bulldozer_Mechanic3;
    }

    public void setRepair_time_Bulldozer_Mechanic3(float repair_time_Bulldozer_Mechanic3) {
        this.repair_time_Bulldozer_Mechanic3 = repair_time_Bulldozer_Mechanic3;
    }

    public float getRepair_time_Excavator_Mechanic6() {
        return repair_time_Excavator_Mechanic6;
    }

    public void setRepair_time_Excavator_Mechanic6(float repair_time_Excavator_Mechanic6) {
        this.repair_time_Excavator_Mechanic6 = repair_time_Excavator_Mechanic6;
    }

    public float getRepair_time_Bulldozer_Mechanic6() {
        return repair_time_Bulldozer_Mechanic6;
    }

    public void setRepair_time_Bulldozer_Mechanic6(float repair_time_Bulldozer_Mechanic6) {
        this.repair_time_Bulldozer_Mechanic6 = repair_time_Bulldozer_Mechanic6;
    }

    public float getRepair_time_Excavator_All() {
        return repair_time_Excavator_All;
    }

    public void setRepair_time_Excavator_All(float repair_time_Excavator_All) {
        this.repair_time_Excavator_All = repair_time_Excavator_All;
    }

    public float getRepair_time_Bulldozer_ALL() {
        return repair_time_Bulldozer_ALL;
    }

    public void setRepair_time_Bulldozer_ALL(float repair_time_Bulldozer_ALL) {
        this.repair_time_Bulldozer_ALL = repair_time_Bulldozer_ALL;
    }

    public float getPay_Mechanic3() {
        return pay_Mechanic3;
    }

    public void setPay_Mechanic3(float pay_Mechanic3) {
        this.pay_Mechanic3 = pay_Mechanic3;
    }

    public float getPay_Mechanic6() {
        return pay_Mechanic6;
    }

    public void setPay_Mechanic6(float pay_Mechanic6) {
        this.pay_Mechanic6 = pay_Mechanic6;
    }

    public float getPay_Brigade() {
        return pay_Brigade;
    }

    public void setPay_Brigade(float pay_Brigade) {
        this.pay_Brigade = pay_Brigade;
    }

    public float getWork() {
        return work;
    }

    public void setWork(float work) {
        this.work = work;
    }

    public float getSleep() {
        return sleep;
    }

    public void setSleep(float sleep) {
        this.sleep = sleep;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public boolean isUse_Mechanic3() {
        return use_Mechanic3;
    }

    public void setUse_Mechanic3(boolean use_Mechanic3) {
        this.use_Mechanic3 = use_Mechanic3;
    }

    public boolean isUse_Mechanic6() {
        return use_Mechanic6;
    }

    public void setUse_Mechanic6(boolean use_Mechanic6) {
        this.use_Mechanic6 = use_Mechanic6;
    }

    @Override
    public String toString() {
        return "Form{" +
                "mathematical_expectation_Excavator=" + mathematical_expectation_Excavator +
                ", mathematical_expectation_Bulldozer=" + mathematical_expectation_Bulldozer +
                ", profit_Excavator=" + profit_Excavator +
                ", profit_Bulldozer=" + profit_Bulldozer +
                ", loss_Excavator=" + loss_Excavator +
                ", loss_Bulldozer=" + loss_Bulldozer +
                ", repair_time_Excavator_Mechanic3=" + repair_time_Excavator_Mechanic3 +
                ", repair_time_Bulldozer_Mechanic3=" + repair_time_Bulldozer_Mechanic3 +
                ", repair_time_Excavator_Mechanic6=" + repair_time_Excavator_Mechanic6 +
                ", repair_time_Bulldozer_Mechanic6=" + repair_time_Bulldozer_Mechanic6 +
                ", repair_time_Excavator_All=" + repair_time_Excavator_All +
                ", repair_time_Bulldozer_ALL=" + repair_time_Bulldozer_ALL +
                ", pay_Mechanic3=" + pay_Mechanic3 +
                ", pay_Mechanic6=" + pay_Mechanic6 +
                ", work=" + work +
                ", sleep=" + sleep +
                ", days=" + days +
                ", use_Mechanic3=" + use_Mechanic3 +
                ", use_Mechanic6=" + use_Mechanic6 +
                '}';
    }
}
