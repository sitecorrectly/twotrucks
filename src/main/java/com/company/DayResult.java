package com.company;

public class DayResult {
    private float workExcavator;
    private float workBulldozer;
    private float idleExcavator;
    private float idleBulldozer;
    private float repairExcavator;
    private float repairBulldozer;
    private float money;

    public float getWorkExcavator() {
        return workExcavator;
    }

    public float getWorkBulldozer() {
        return workBulldozer;
    }

    public float getIdleExcavator() {
        return idleExcavator;
    }

    public float getIdleBulldozer() {
        return idleBulldozer;
    }

    public float getRepairExcavator() {
        return repairExcavator;
    }

    public float getRepairBulldozer() {
        return repairBulldozer;
    }

    public float getMoney() {
        return money;
    }

    public DayResult(float workExcavator, float workBulldozer, float idleExcavator, float idleBulldozer, float repairExcavator, float repairBulldozer, float money) {
        this.workExcavator = workExcavator / 60f;
        this.workBulldozer = workBulldozer / 60f;
        this.idleExcavator = idleExcavator / 60f;
        this.idleBulldozer = idleBulldozer / 60f;
        this.repairExcavator = repairExcavator / 60f;
        this.repairBulldozer = repairBulldozer / 60f;
        this.money = money;
    }
}
