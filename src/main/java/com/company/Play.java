package com.company;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;

public class Play {
    private static final Logger logger = LogManager.getLogger(Play.class);
    Random random = new Random();
    // Длина дня в минутах
    int workDayDuration;
    // Инициализация
    int work_time_Excavator = 0;
    int work_time_Bulldozer = 0;
    int idle_time_Excavator = 0;
    int idle_time_Bulldozer = 0;
    int repair_time_Excavator = 0;
    int repair_time_Bulldozer = 0;

    // Состояние техники
    boolean Excavator_OK = true;
    boolean Bulldozer_OK = true;

    private Form form;
    private float money = 0;

    public Play(Form form) {
        this.form = form;
        workDayDuration = (int) (form.getWork() * 60f); //hour * 60 minutes
    }

    public void step() {
        while (!finish()) {
            // Если вся техника в порядке - продолжаем
            if (Excavator_OK && Bulldozer_OK)
                work();
            // Если что-то сломано - чиним
            if (!(Excavator_OK && Bulldozer_OK))
                repair();
        }
        //logger.debug("Excavator w+i=" + work_time_Excavator + "+" + idle_time_Excavator + "=" + (work_time_Excavator + idle_time_Excavator));
        //logger.debug("Bulldozer w+i=" + work_time_Bulldozer + "+" + idle_time_Bulldozer + "=" + (work_time_Bulldozer + idle_time_Bulldozer));
        //logger.debug("Money " + money);
    }

    public DayResult clean() {
        DayResult result = new DayResult(work_time_Excavator,
                work_time_Bulldozer, idle_time_Excavator, idle_time_Bulldozer, repair_time_Excavator, repair_time_Bulldozer, money);
        // Сбрасываем состояние
        work_time_Excavator = 0;
        work_time_Bulldozer = 0;
        idle_time_Excavator = 0;
        idle_time_Bulldozer = 0;
        repair_time_Excavator = 0;
        repair_time_Bulldozer = 0;

        Excavator_OK = true;
        Bulldozer_OK = true;

        money = 0;
        return result;
    }

    // Сколько-то отработали и либо закончили день, либо сломались.
    private void work() {
        if (work_time_Excavator + idle_time_Excavator < workDayDuration)
            work_time_Excavator += cutTime(workDayDuration, work(form.getMathematical_expectation_Excavator()), work_time_Excavator + idle_time_Excavator);
        if (work_time_Excavator + idle_time_Excavator < workDayDuration)
            Excavator_OK = false;

        if (work_time_Bulldozer + idle_time_Bulldozer < workDayDuration)
            work_time_Bulldozer += cutTime(workDayDuration, work(form.getMathematical_expectation_Bulldozer()), work_time_Bulldozer + idle_time_Bulldozer);
        if (work_time_Bulldozer + idle_time_Bulldozer < workDayDuration)
            Bulldozer_OK = false;
    }

    // Если отработали норму, завершаем день
    private boolean finish() {
        // День закончился
        if (work_time_Excavator + idle_time_Excavator >= workDayDuration && work_time_Bulldozer + idle_time_Bulldozer >= workDayDuration) {
            // Прибыль
            money += ((float) work_time_Excavator) / 60f * form.getProfit_Excavator();
            money += ((float) work_time_Bulldozer) / 60f * form.getProfit_Bulldozer();
            // Убытки
            money -= ((float) idle_time_Excavator) / 60f * form.getLoss_Excavator();
            money -= ((float) idle_time_Bulldozer) / 60f * form.getLoss_Bulldozer();
            return true;
        }
        return false;
    }

    private void repair() {
        int time;
        // Чиним только одну еденицу за раз (для расчёта времени)
        /**
         * 1. Определить что сломано
         * 1.1 Сломано всё
         * 1.2 Сломано только что-то одно
         * 2. Починить
         * 2.1 Доступен один рабочий
         * 2.2 Доступно 2 рабочих
         */


        // 1
        // 1.1
        if ((!Bulldozer_OK) && (!Excavator_OK)) {
            // Можно чинить только одним рабочим, а можно всеми.
            // Определить кто раньше сломался
            if (work_time_Excavator + idle_time_Excavator > work_time_Bulldozer + idle_time_Bulldozer) {
                // Первым сломался бульдозер
                time = repairBulldozer();
                idle_time_Bulldozer += time;
                idle_time_Excavator += cutTime(workDayDuration, time, work_time_Excavator + idle_time_Excavator); // экскаватор мог не доработать 5 мин до конца смены и сломаться, а бульдозер 15 мин до конца и сломаться, его просто должен быть не больше 5 минут до конца смены
                repair_time_Bulldozer += time;
                money -= ((float) form.getPay_Brigade() / 60f) * time;
                Bulldozer_OK = true;
            } else {
                // Первым сломался экскаватор
                time = repairExcavator();
                idle_time_Bulldozer += cutTime(workDayDuration, time, work_time_Bulldozer + idle_time_Bulldozer);
                idle_time_Excavator += time;
                repair_time_Excavator += time;
                money -= ((float) form.getPay_Brigade() / 60f) * time;
                Excavator_OK = true;
            }

        }
        //1.2
        if (!Excavator_OK) {
            time = repairExcavator();
            idle_time_Excavator += time;
            repair_time_Excavator += time;
            money -= ((float) form.getPay_Brigade() / 60f) * time;
            Excavator_OK = true;
        }
        if (!Bulldozer_OK) {
            time = repairBulldozer();
            idle_time_Bulldozer += time;
            repair_time_Bulldozer += time;
            money -= ((float) form.getPay_Brigade() / 60f) * time;
            Bulldozer_OK = true;
        }


    }

    // Время починки экскаватора
    public int repairExcavator() {
        int time = 0;
        if (form.isUse_Mechanic3() && form.isUse_Mechanic6() && form.getRepair_time_Excavator_All() != -1) {// Доступны все
            time = cutTime(workDayDuration, work(form.getRepair_time_Excavator_All()), work_time_Excavator + idle_time_Excavator);
            money -= (((float) time / 60f) * form.getPay_Mechanic3() + ((float) time / 60f) * form.getPay_Mechanic6());
        } else if (form.isUse_Mechanic3() && form.getRepair_time_Excavator_Mechanic3() != -1) {// Доступен только один
            time = cutTime(workDayDuration, work(form.getRepair_time_Excavator_Mechanic3()), work_time_Excavator + idle_time_Excavator);
            money -= ((float) time / 60f) * form.getPay_Mechanic3();
        } else if (form.isUse_Mechanic6() && form.getRepair_time_Excavator_Mechanic6() != -1) {// Доступен только один
            time = cutTime(workDayDuration, work(form.getRepair_time_Excavator_Mechanic6()), work_time_Excavator + idle_time_Excavator);
            money -= ((float) time / 60f) * form.getPay_Mechanic6();
        } else
            time = cutTime(workDayDuration, workDayDuration, work_time_Excavator + idle_time_Excavator);
        return time;
    }

    // Время починки бульдозера
    public int repairBulldozer() {
        int time = 0;
        if (form.isUse_Mechanic3() && form.isUse_Mechanic6() && form.getRepair_time_Bulldozer_ALL() != -1) {// Доступны все
            time = cutTime(workDayDuration, work(form.getRepair_time_Bulldozer_ALL()), work_time_Bulldozer + idle_time_Bulldozer);
            money -= (((float) time / 60f) * form.getPay_Mechanic3() + ((float) time / 60f) * form.getPay_Mechanic6());
        } else if (form.isUse_Mechanic3() && form.getRepair_time_Bulldozer_Mechanic3() != -1) {// Доступен только один
            time = cutTime(workDayDuration, work(form.getRepair_time_Bulldozer_Mechanic3()), work_time_Bulldozer + idle_time_Bulldozer);
            money -= ((float) time / 60f) * form.getPay_Mechanic3();
        } else if (form.isUse_Mechanic6() && form.getRepair_time_Bulldozer_Mechanic6() != -1) {// Доступен только один
            time = cutTime(workDayDuration, work(form.getRepair_time_Bulldozer_Mechanic6()), work_time_Bulldozer + idle_time_Bulldozer);
            money -= ((float) time / 60f) * form.getPay_Mechanic6();
        } else
            time = cutTime(workDayDuration, workDayDuration, work_time_Bulldozer + idle_time_Bulldozer);
        return time;

    }

    int cutTime(int maxValue, int value, int currentValue) {
        if (currentValue + value > maxValue)
            return maxValue - currentValue;
        else
            return value;
    }

    int work(double mathematicalExpectation) {
        return (int) getNext(mathematicalExpectation * 60, random);
    }

    double getNext(Double mathematicalExpectation, Random random) {
        double alpha = 1d / mathematicalExpectation;
        double current = random.nextDouble();
        return (-(1.0 / alpha)) * Math.log(current);
    }

    public int getWorkDayDuration() {
        return workDayDuration;
    }

    public int getWork_time_Excavator() {
        return work_time_Excavator;
    }


    public int getWork_time_Bulldozer() {
        return work_time_Bulldozer;
    }


    public int getIdle_time_Excavator() {
        return idle_time_Excavator;
    }


    public int getIdle_time_Bulldozer() {
        return idle_time_Bulldozer;
    }

    public float getMoney() {
        return money;
    }
}
