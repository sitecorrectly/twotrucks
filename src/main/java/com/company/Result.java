package com.company;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.List;

public class Result {
    final String FORMAT_hour = "%2.2f";
    final String FORMAT_money = "%, 7.2f";

    public byte[] create(List<DayResult> dayResults, Form form) {
        List<String> lines = null;
        try {
            lines = Files.readAllLines(FileSystems.getDefault().getPath("html/result.html"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (lines == null) {
            try {
                return "Error read pattern".getBytes("utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return new byte[0];
            }
        }
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        for (String line : lines) {
            try {
                os.write(pattern(line, dayResults, form).getBytes("utf-8"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return os.toByteArray();
    }

    private String pattern(String original, List<DayResult> dayResults, Form form) {
        String str = original;
        // Заполняем время
        str = str.replace("{average_workExcavator_time}", String.format(FORMAT_hour, dayResults.stream().mapToDouble(d -> d.getWorkExcavator()).average().orElse(0)));
        str = str.replace("{average_workBulldozer_time}", String.format(FORMAT_hour, dayResults.stream().mapToDouble(d -> d.getWorkBulldozer()).average().orElse(0)));
        str = str.replace("{average_repairExcavator_time}", String.format(FORMAT_hour, dayResults.stream().mapToDouble(d -> d.getRepairExcavator()).average().orElse(0)));
        str = str.replace("{average_repairBulldozer_time}", String.format(FORMAT_hour, dayResults.stream().mapToDouble(d -> d.getRepairBulldozer()).average().orElse(0)));
        str = str.replace("{average_idleExcavator_time}", String.format(FORMAT_hour, dayResults.stream().mapToDouble(d -> d.getIdleExcavator()).average().orElse(0)));
        str = str.replace("{average_idleBulldozer_time}", String.format(FORMAT_hour, dayResults.stream().mapToDouble(d -> d.getIdleBulldozer()).average().orElse(0)));
        str = str.replace("{average_Brigade_work_time}",
                String.format(
                        FORMAT_hour,
                        dayResults.stream().mapToDouble(d -> d.getRepairExcavator()).average().orElse(0)
                                + dayResults.stream().mapToDouble(d -> d.getRepairBulldozer()).average().orElse(0)));
        // Заполняем деньги
        str = str.replace("{average_workExcavator_money}", String.format(FORMAT_money, (dayResults.stream().mapToDouble(d -> d.getWorkExcavator()).average().orElse(0) * form.getProfit_Excavator())));
        str = str.replace("{average_workBulldozer_money}", String.format(FORMAT_money, (dayResults.stream().mapToDouble(d -> d.getWorkBulldozer()).average().orElse(0) * form.getProfit_Bulldozer())));
        str = str.replace("{average_repairExcavator_money}", String.format(FORMAT_money, (dayResults.stream().mapToDouble(d -> d.getRepairExcavator()).average().orElse(0))));
        str = str.replace("{average_repairBulldozer_money}", String.format(FORMAT_money, (dayResults.stream().mapToDouble(d -> d.getRepairBulldozer()).average().orElse(0))));
        str = str.replace("{average_idleExcavator_money}", String.format(FORMAT_money, (dayResults.stream().mapToDouble(d -> d.getIdleExcavator()).average().orElse(0) * form.getLoss_Excavator())));
        str = str.replace("{average_idleBulldozer_money}", String.format(FORMAT_money, (dayResults.stream().mapToDouble(d -> d.getIdleBulldozer()).average().orElse(0) * form.getLoss_Bulldozer())));
        str = str.replace("{average_Brigade_work_money}",
                String.format(
                        FORMAT_hour,
                        (dayResults.stream().mapToDouble(d -> d.getRepairExcavator()).average().orElse(0)
                                + dayResults.stream().mapToDouble(d -> d.getRepairBulldozer()).average().orElse(0) * form.getPay_Brigade())));
        // Заполняем итоги
        str = str.replace("{average_profit}", String.format(FORMAT_hour, dayResults.stream().mapToDouble(d -> d.getMoney()).average().orElse(0)));

        if (str.contains("{table}"))
            str = str.replace("{table}", createTable(dayResults, form));

        /**
         * <tr>
         *                     <td>
         *                         1
         *                     </td>
         *                     <td>
         *                         100
         *                     </td>
         *                     <td>
         *                         200
         *                     </td>
         *                     <td>
         *                         300
         *                     </td>
         *                     <td>
         *                         100
         *                     </td>
         *                     <td>
         *                         200
         *                     </td>
         *                     <td>
         *                         100
         *                     </td>
         *                     <td>
         *                         200
         *                     </td>
         *                 </tr>
         */

        //original.replace(  "average_workExcavator_time","{123}");
        return str;
    }

    private String createTable(List<DayResult> dayResults, Form form) {
        StringBuffer stringBuffer = new StringBuffer();
        int day = 0;
        for (DayResult dayResult : dayResults) {
            stringBuffer.append("<tr>");
            stringBuffer.append("<td>").append(++day).append("</td>");
            stringBuffer.append("<td>").append(String.format(FORMAT_hour, dayResult.getWorkExcavator())).append("</td>");
            stringBuffer.append("<td>").append(String.format(FORMAT_hour, dayResult.getWorkBulldozer())).append("</td>");
            stringBuffer.append("<td>").append(String.format(FORMAT_hour, dayResult.getIdleExcavator())).append("</td>");
            stringBuffer.append("<td>").append(String.format(FORMAT_hour, dayResult.getIdleBulldozer())).append("</td>");
            stringBuffer.append("<td>").append(String.format(FORMAT_hour, dayResult.getRepairExcavator())).append("</td>");
            stringBuffer.append("<td>").append(String.format(FORMAT_hour, dayResult.getRepairBulldozer())).append("</td>");
            stringBuffer.append("<td>").append(String.format(FORMAT_money, dayResult.getMoney())).append("</td>");
            stringBuffer.append("</tr>");
        }
        return stringBuffer.toString();
    }
}
