package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TestClass {
    public static void main(String[] args) {
        final double alpha = 1d / 4d;
        final String FORMAT = "%f\t%f";

        Random random = new Random();
        for (int i = 0; i < 20; i++) {

            double current = random.nextDouble();

            // Формула из методички
            double resultMethodical = (-(1.0 / alpha)) * Math.log(current);

            // Формула по указанной ссылке
            double resultOther = Math.log(1 - current) / (-alpha);

            System.out.println(
                    String.format(FORMAT, resultMethodical, resultOther));
        }

        List<Double> avg = new ArrayList(1000);
        for (int i = 0; i < 10000; i++) {
            avg.add(getNext(alpha, random));
            if (i % 1000 == 0)
                System.out.println(avg.stream().mapToDouble(Double::doubleValue)
                        .average()
                        .orElse(Double.NaN));
        }

    }

    static double getNext(Double alpha, Random random) {
        double current = random.nextDouble();
        return (-(1.0 / alpha)) * Math.log(current);
    }
}
