package com.company;

import org.apache.commons.fileupload.FileItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class FormParser {
    private static final Logger logger = LogManager.getLogger(FormParser.class);

    public static Form parse(List<FileItem> items) {
        Form form = new Form();
        reparse(items, form);
        return form;
    }

    public static Form reparse(List<FileItem> items, Form form) {
        for (FileItem item : items) {
//            logger.debug(item);
//            if (item.getContentType() != null)
//                logger.debug("Content-Type: " + item.getContentType());
//            logger.debug("Field: " + item.getFieldName());
//            logger.debug("Data:  "+new String(item.get()));
            if (item.isFormField()) {
                logger.debug(String.format("Key %25s value %s", item.getFieldName(), new String(item.get())));
                putBasicParameter(item, form);
            } else {
                //putFile(item, form);
            }
        }
        //putDifficultParameters(items, form);
        return form;
    }

    private static Form putBasicParameter(FileItem item, Form form) {
        String name = item.getFieldName();
        String value = new String(item.get());
        Float floatValue = -1f;
        try {
            floatValue = Float.parseFloat(value);
        } catch (NumberFormatException e) {
        }

        switch (name) {
            case "ME_E":
                form.setMathematical_expectation_Excavator(floatValue);
                break;
            case "ME_B":
                form.setMathematical_expectation_Bulldozer(floatValue);
                break;
            case "Profit_E":
                form.setProfit_Excavator(floatValue);
                break;
            case "Profit_B":
                form.setProfit_Bulldozer(floatValue);
                break;
            case "Loss_E":
                form.setLoss_Excavator(floatValue);
                break;
            case "Loss_B":
                form.setLoss_Bulldozer(floatValue);
                break;
            case "Repair_M3_E":
                form.setRepair_time_Excavator_Mechanic3(floatValue);
                break;
            case "Repair_M3_B":
                form.setRepair_time_Bulldozer_Mechanic3(floatValue);
                break;
            case "Repair_M6_E":
                form.setRepair_time_Excavator_Mechanic6(floatValue);
                break;
            case "Repair_M6_B":
                form.setRepair_time_Bulldozer_Mechanic6(floatValue);
                break;
            case "Repair_All_E":
                form.setRepair_time_Excavator_All(floatValue);
                break;
            case "Repair_All_B":
                form.setRepair_time_Bulldozer_ALL(floatValue);
                break;
            case "Pay_M3":
                form.setPay_Mechanic3(floatValue);
                break;
            case "Pay_M6":
                form.setPay_Mechanic6(floatValue);
                break;
            case "Pay_brigade":
                form.setPay_Brigade(floatValue);
                break;
            case "workTime":
                form.setWork(floatValue);
                break;
            case "sleepTime":
                form.setSleep(floatValue);
                break;
            case "work_M3":
                form.setUse_Mechanic3(value.equals("true"));
                break;
            case "work_M6":
                form.setUse_Mechanic6(value.equals("true"));
                break;
            case "days":
                try {
                    form.setDays(Integer.parseInt(value));
                } catch (NumberFormatException e) {
                }
                break;
            default:
                logger.warn("Key " + name + " not found.");
        }

        return form;
    }
}
