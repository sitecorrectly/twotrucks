package com.company.web;

import com.company.*;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.RequestContext;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class MainHttpHandler extends CustomHttpHandler {
    private static final Logger logger = LogManager.getLogger(MainHttpHandler.class);

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        Headers h = httpExchange.getResponseHeaders();
        h.add("charset", "utf-8");

        if ("POST".equals(httpExchange.getRequestMethod())) ;
        if ("GET".equals(httpExchange.getRequestMethod()))
            createResponse(httpExchange, 400, "<!doctype html><html lang=\"ru\"><head><meta charset=\"utf-8\"></head><body>" + "Use POST.".replace("\n", "<br>") + "</body></html>");

        DiskFileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setSizeMax(1024000);
        RequestContext request = new RequestContext() {

            @Override
            public String getCharacterEncoding() {
                return "UTF-8";
            }

            @Override
            public int getContentLength() {
                return 0; //tested to work with 0 as return
            }

            @Override
            public String getContentType() {
                return httpExchange.getRequestHeaders().getFirst("Content-type");
            }

            @Override
            public InputStream getInputStream() throws IOException {
                return httpExchange.getRequestBody();
            }
        };
        try {
            // Parse the request
            List<FileItem> items = upload.parseRequest(request);

            Form form = FormParser.parse(items);
            logger.debug(form.toString());
            Play play = new Play(form);
            List<DayResult> dayResults = new ArrayList<>();
            for (int i = 0; i < form.getDays(); i++) {
                play.step();
                dayResults.add(play.clean());
            }

            OutputStream outputStream = httpExchange.getResponseBody();
            httpExchange.getResponseHeaders().add("Content-Type", "text/html; charset=utf-8");
            byte[] bs = new Result().create(dayResults, form);
            httpExchange.sendResponseHeaders(200, bs.length);
            outputStream.write(bs);
            outputStream.flush();
            outputStream.close();
            return;
        } catch (FileUploadException e) {
            logger.error(e);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            ex.printStackTrace();
        }

        createResponse(httpExchange, 500, "Error?!");
    }
}
