package com.company.web;

import com.sun.net.httpserver.HttpServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class Server {
    private static final Logger logger = LogManager.getLogger(Server.class);
    ThreadPoolExecutor threadPoolExecutor;
    HttpServer server;

    private String host = "0.0.0.0";
    private int port = 8080;

    public Server() {
        threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(2);
        try {
            server = HttpServer.create(new InetSocketAddress(host, port), 0);
            server.createContext("/", new RootHttpHandler());
            server.createContext("/process", new MainHttpHandler());
            //server.createContext("/process", new ParseExcelPatternHttpHandle());
            server.setExecutor(threadPoolExecutor);
            server.start();
            logger.info("root.web.Server started on " + host + ":" + port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void STOP() {
        server.stop(0);
        threadPoolExecutor.shutdownNow();
    }

}
