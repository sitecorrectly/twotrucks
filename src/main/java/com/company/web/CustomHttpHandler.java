package com.company.web;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;

public abstract class CustomHttpHandler implements HttpHandler {
    private static final Logger logger = LogManager.getLogger(CustomHttpHandler.class);

    /**
     * Возвращает ответ клиенту.
     *
     * @param httpExchange    HTTP request received
     * @param ResponsePayload Отправляемая страница (HTML код)
     * @param code            Код ответа
     * @throws IOException Выкидывает, когда возникли ошибки при отправке.
     */
    protected void createResponse(HttpExchange httpExchange, int code, String ResponsePayload) throws IOException {
        logger.debug("Return: " + code + " " + httpExchange.getRequestURI());
        Headers h = httpExchange.getResponseHeaders();
        h.add("charset", "utf-8");
        OutputStream outputStream = httpExchange.getResponseBody();

        byte[] bs = new byte[0];
        try {
            bs = ResponsePayload.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        httpExchange.sendResponseHeaders(code, bs.length);

        outputStream.write(bs);
        outputStream.flush();
        outputStream.close();
    }

    /**
     * Возвращает ошибку с кодом 500.
     *
     * @param httpExchange    HTTP request received
     * @param ResponsePayload Отправляемая страница (HTML код)
     * @throws IOException Выкидывает, когда возникли ошибки при отправке.
     */
    protected void createErrorResponse(HttpExchange httpExchange, String ResponsePayload) throws IOException {
        createResponse(httpExchange, 500, "<!doctype html><html lang=\"ru\"><head><meta charset=\"utf-8\"></head><body>" + ResponsePayload.replace("\n", "<br>") + "</body></html>");
    }

    /**
     * Возвращает ответ клиенту.
     *
     * @param httpExchange HTTP request received
     * @param file         Отправляемый файл
     * @throws IOException Выкидывает, когда возникли ошибки при отправке.
     */
    protected void createResponse(HttpExchange httpExchange, File file) throws IOException {
        logger.debug("Return: " + 200 + " " + httpExchange.getRequestURI());
        OutputStream outputStream = httpExchange.getResponseBody();
        Files.readAllBytes(file.toPath());

        byte[] bs = Files.readAllBytes(file.toPath());
        httpExchange.sendResponseHeaders(200, bs.length);

        outputStream.write(bs);
        outputStream.flush();
        outputStream.close();
    }

}
