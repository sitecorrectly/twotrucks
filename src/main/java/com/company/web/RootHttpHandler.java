package com.company.web;

import com.sun.net.httpserver.HttpExchange;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

public class RootHttpHandler extends CustomHttpHandler {
    private static final Logger logger = LogManager.getLogger(RootHttpHandler.class);

    @Override
    public void handle(HttpExchange httpExchange) {
        String url = httpExchange.getRequestURI().toString();
        logger.debug("Url " + url);
        if (url.equals("/"))
            url = "index.html";
        if ("POST".equals(httpExchange.getRequestMethod()))
            try {
                createResponse(httpExchange, 418, "Page " + url + " not found.");
            } catch (IOException io_e) {
                logger.error(io_e.getMessage());
                io_e.printStackTrace();
            }

        if ("GET".equals(httpExchange.getRequestMethod())) {
            try {
                File requestFile = new File("html/" + url);
                if (requestFile.exists()) {
                    switch (Optional.ofNullable(requestFile.getName())
                            .filter(f -> f.contains("."))
                            .map(f -> f.substring(requestFile.getName().lastIndexOf(".") + 1)).orElse("null")) {
                        case "js":
                            httpExchange.getResponseHeaders().add("Content-Type", "application/javascript; charset=utf-8");
                            break;
                        case "html":
                            httpExchange.getResponseHeaders().add("Content-Type", "text/html; charset=utf-8");
                            break;
                    }
                    createResponse(httpExchange, requestFile);
                } else
                    createResponse(httpExchange, 404, "Page " + url + " not found.");
            } catch (IOException io_e) {
                logger.error(io_e);
                logger.trace(io_e);
            }
        }
    }
}
